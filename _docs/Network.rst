****************
Network Commands
****************
.. highlight:: bash

Hostname.
=========
Set.
----
::

    sudo hostnamectl set-hostname vm7.orgDomain

View.
-----
::

    hostnamectl status
    # or
    cat /etc/hostname

IP.
===
Add a route temporarily (until reboot).
---------------------------------------
::

    ip route add 192.168.2.1/23 via 10.11.12.3

Add an IP address to an interface temporarily (until reboot).
-------------------------------------------------------------
::

    sudo ip address add 192.168.1.1/24 dev eth0

Bring interfaces up and down.
-----------------------------
.. highlight:: none

::

    sudo ip link set eth0 up
    sudo ip link set eth0 down

Delete a route temporarily (until reboot).
------------------------------------------
.. highlight:: bash

::

    sudo ip route delete 192.168.2.1/23 via 10.11.12.3
    # or
    sudo ip route delete 192.168.2.1/23

Show ARP cache (MAC addresses).
-------------------------------
::

    ip -c neighbor show

Show information for all interfaces.
------------------------------------
::

    ip -c link show

Show ip addresses allocated to interfaces.
------------------------------------------
::

    ip -c address show

Show statistics of an interface.
--------------------------------
::

    ip -c -h -s link show eth0

* Options c and h add color and human-readable values to output.

Route.
======
Add route temporarily (until reboot).
-------------------------------------
::

    sudo route add -net 192.168.0.0 netmask 255.255.255.0 gw 192.168.0.1
    # Add default gateway.
    sudo route add default gw 192.168.0.1

Delete route temporarily (until reboot).
------------------------------------------
::

    sudo route del -net 192.168.0.0 netmask 255.255.255.0
    # Delete default gateway.
    sudo route del default

Show routing table.
-------------------
::

    route

Ss.
===
Useful options.
---------------

-4  IPv4 sockets only.
-a  Listening and non-listening sockets (established for TCP).
-l  Listening sockets only.
-m  Socket memory usage.
-n  Don't resolve numeric adresses/ports.
-o  Options (i. e. state established).
-p  Process using socket.
-t  TCP sockets.
-u  UDP sockets.
-x  Unix domain sockets.
-Z  Process and security context (SELinux).

Traceroute.
===========
See route to target host. ::

    traceroute targetHost

* To bypass firewalls that block ICMP traffic try options ::

    --udp
    # or
    --tcp

**Note:** These commands were tested on RHEL 7 unless otherwise specified.
