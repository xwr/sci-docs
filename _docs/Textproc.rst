***************
Text processing
***************
.. highlight:: bash

Convert file encoding.
======================
Get available encodings. ::

	iconv -l

Convert text from the ISO 8859-15 character encoding to UTF-8. ::

	iconv -f ISO-8859-15 -t UTF-8 < input.txt > output.txt

Get number of lines in targetFile.
==================================
::

	wc -l targetFile | grep -Eo '[0-9]+'

Read csv file and extract certain columns in certain order.
===========================================================
::

	awk -F ',' '{print $3 "," $1}' a1.csv > b2.csv

* Saves output to file b2.csv.
* Columns are $1, $2, $x.
* Default fieldsep if not set is space.

Remove duplicate lines.
=======================
Comparing whole lines (order preserved in output).
--------------------------------------------------
::

	awk '!seen[$0]++' target.csv

Comparing whole lines (order not preserved in output).
------------------------------------------------------
::

	sort -u target.csv

* -f ignores case for comparing.

Sort first column alphabetically, second numerically.
=====================================================
::

	sort -k1,1 -k2,2nr

* Last r causes reversed outpur.
* Default fieldsep is space.
