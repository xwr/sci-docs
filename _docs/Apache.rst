******
Apache
******
.. highlight:: none

Clear log file when it grows over 300MB.
========================================
Add directive to file /etc/logrotate.d/apache2. ::

    /var/log/apache2/rewrite_log {
        copytruncate
        rotate 0
        size=+300M
        notifempty
        missingok
    }

* This method doesn't require reloading httpd services or creating a new file.

