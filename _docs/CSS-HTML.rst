**************
CSS/HTML notes
**************
.. highlight:: css

CSS.
====
Reference a class.
------------------
::

	.className

Reference an id.
----------------
::

	#id

HTML.
=====
Assign class to page element.
-----------------------------
::

    <span class="className"> ... </span>

* A class can be attribute of any tag.

Assign id to page element.
--------------------------
::

    <div id="anID"> ... </div>

* Has to be unique.
* Can be attribute of any tag.

HTML5 version identifier.
-------------------------
First line of document. ::

    <!DOCTYPE html>

Use style from external css file.
---------------------------------
::

    <link rel="stylesheet" href="targetFile.css">
