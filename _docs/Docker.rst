******
Docker
******
.. highlight:: bash

Build image.
============
::

    sudo docker build -t imgName /path/to/dockerfile/dir

Delete all containers.
======================
::

    sudo docker rm "$(sudo docker ps -a -q)"

Delete all images.
==================
::

    sudo docker rmi "$(sudo docker images -q)"

Edit file from container volume.
================================
Copy it to local machine (current directory) and make changes. ::

    sudo docker cp containerName:/path/to/file.ext .

Move the edited file back to the volume. ::

    sudo docker cp file.ext containerName:/path/to/file.ext

Get installed images.
=====================
::

    sudo docker images

Get running containers.
=======================
::

    sudo docker ps

.. highlight:: none

Kill running container.
=======================
::

    sudo docker kill containerId

.. highlight:: bash

Run image.
==========
::

    sudo docker run [OPTIONS] IMAGE [COMMAND] [COMMAND ARGS]

Common options.
===============

-i  Interactive (for shells).
--rm  Automatically remove the container when it exits.

::

    -e VAR=value  # Set environment variable.
    --expose portNum  # Expose port.
    --network="host"  # Use host network interfaces.
    -v /host/vol:/container/vol  # Mount a host volume in the container.
    -v /target:/dest:Z  # Grant selinux permission to host dir (target) and contents.

