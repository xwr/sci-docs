**************
Docker compose
**************
.. highlight:: bash

Build.
======
Build images defined in yml file.
---------------------------------
::

    sudo docker-compose build

Build images and start services.
--------------------------------
::

    sudo docker-compose up --build

* Option -f /path/to/yml can be used to specify the path to the definition file when running commands from a different dir.

Get info on running containers.
===============================
::

    sudo docker-compose ps

.. highlight:: none

Open shell in running container.
================================
::

    sudo docker-compose exec serviceName bash

* serviceName has to match the one in docker-compose.yml.

.. highlight:: bash

Start services in detached mode (bg process).
=============================================
::

    sudo docker-compose up -d

When using this mode output from containers can be seen by running: ::

    sudo docker-compose logs

Start specific service only.
============================
::

    sudo docker-compose up --no-start
    sudo docker-compose start serviceName

Stop all running services.
==========================
::

    sudo docker-compose down

