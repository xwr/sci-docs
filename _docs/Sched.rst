***************
Scheduled tasks
***************
.. highlight:: bash

Anacron jobs.
=============

* Files under /etc/cron.xyz (i. e. daily).
* These files have to be executable, owned by root, not writable by group or other.
* Symlinks have to point to files owned by root.
* File names must be made up of letters, digits, underscores or hyphens only.

Test (manually run) scripts. ::

    sudo run-parts /etc/cron.daily

* Tested on Fedora 29.
