#!/bin/bash

files="$(pwd)/files.inotify"
cd ../.. || return

inotifywait -r -m -e create -e delete -e modify -e move \
--fromfile "$files" | while read -r line;
    do
        # uncomment to log triggering events
        # echo "$line" >> inotify.log
        bash -c 'source activate sphinx ; make html &> makehtml.log'
    done

